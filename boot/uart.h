/*
 * File  : uart.h
 * Author: Cirilo Bernardo (cirilo.bernardo@gmail.com)
 * Date  : 15 Aug 2020
 * Description: UART driver
 *
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Provides Microprocessor Configuration settings.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#ifndef UART_H
#define UART_H

#include <stdbool.h>
#include <stdint.h>

typedef struct
{
    uint32_t rx_overflow : 1;           /* rx buffer overrun */
    uint32_t tx_underrun : 1;           /* tx buffer can accept data without blocking */
    uint32_t             : 30;          /* currently unused */
} UartStatus_t;


typedef struct
{
    /**
     * @Function Init
     * @detail   Initializes the UART
     * @param    bitrate is the desired communications speed (bit per second)
     * @return   0 on success, else error code
     */
    int32_t (*Init)(uint32_t const bitrate);

   /**
     * @Function Uninit
     * @detail   Shuts down the UART
     */
    void    (*Uninit)(void);

    /**
     * @Function Send
     * @detail   Transmits a message
     * @param    data   [IN] is a pointer to the message
     * @param    length [IN] is the byte length of the message
     * @return   on success the number of bytes transmitted, else error code
     */
    int32_t (*Send)(void const *data, uint32_t const length);

    /**
     * @Function Receive
     * @detail   Receives data from the UART
     * @param    data   [IN] is a pointer to a message buffer
     * @param    length [IN] is the MAX byte length of the buffer
     * @return   on success the number of bytes received, else error code
     */
    int32_t (*Receive)(void *buffer, uint32_t const length);

    /**
     * @Function Status
     * @detail   Returns the UART status flags:
     *           rx_overflow : 1 if the receiver has overflowed (latest data lost)
     *           tx_underrun : 1 if the transmitter can accept more data (not a fault)
     */
    UartStatus_t (*Status)(void);

    /**
     * @Function FlushTx
     * @detail   Blocks until all data in the transmit buffer has been sent.
     */
    void    (*FlushTx)(void);

    /**
     * @Function FlushRx
     * @detail   Clears the contents of the receiver buffer and clears any RX overflow error.
     */
    void    (*FlushRx)(void);

   /**
     * @Function IsActive
     * @return   true if the UART is on
     */
    bool    (*IsActive)(void);
} const UART_Driver;

extern UART_Driver UART2;

void uart_task(void);

#endif /* UART_H */
