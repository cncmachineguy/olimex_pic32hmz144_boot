/*
 * File  : console.h
 * Author: Cirilo Bernardo (cirilo.bernardo@gmail.com)
 * Date  : 16 Aug 2020
 * Description: provides a serial console and print functions
 *
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Provides Microprocessor Configuration settings.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#ifndef CONSOLE_H
#define CONSOLE_H

void console_task(void);

typedef struct
{
    /**
     * @Function Init
     * @detail   Initializes the console
     * @return   0 on success, else error code
     */
    int32_t (*Init)(void);

   /**
     * @Function Uninit
     * @detail   Shuts down the console
     */
    void    (*Uninit)(void);

    /**
     * @Function Puts
     * @detail   Writes a text string to console; line ending is converted to \r\n
     * @param    message [IN] is a pointer to the message to send
     */
    void    (*Puts)(void const *message);

    /**
     * @Function Putc
     * @detail   Writes a byte to the console; no character conversion is performed.
     */
    void    (*Putc)(const uint8_t token);

    /**
     * @Function Getc
     * @return   received character (0..255) else error code.
     */
    int32_t (*Getc)(void);

    /**
     * @Function FlushTx
     * @detail   Blocks until all data in the transmit buffer has been sent.
     */
    void    (*FlushTx)(void);

    /**
     * @Function FlushRx
     * @detail   Clears the contents of the receiver buffer.
     */
    void    (*FlushRx)(void);
} const Console_t;

extern Console_t Console;

#endif /* CONSOLE_H */
