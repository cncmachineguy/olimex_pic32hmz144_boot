/*
 * File  : cfg_interrupts.h
 * Author: Cirilo Bernardo (cirilo.bernardo@gmail.com)
 * Date  : 15 Aug 2020
 * Description: assignments of interrupt priorities and shadow registers
 *              SRS_xxx = one of IPLnSRS, IPLnAUTO, IPLnSOFT
 *              IPL_xxx = Interrupt Priority Level, 0..7
 *              IPS_xxx = Interrupt Priority Sublevel, 0..3
 *
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Provides Microprocessor Configuration settings.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#ifndef CFG_INTERRUPTS_H
#define CFG_INTERRUPTS_H

/* Timer / Counters */
#define SRS_TIMER1      IPL7SRS
#define IPL_TIMER1      7
#define IPS_TIMER1      3

/* UARTs */
/* UART2 : only RX interrupts are used */
#define SRS_UART2_RX    IPL5SRS
#define IPL_UART2_RX    5
#define IPS_UART2_RX    0

#endif /* CFG_INTERRUPTS_H */
