/*
 * File  : sys_util.h
 * Author: Cirilo Bernardo (cirilo.bernardo@gmail.com)
 * Date  : 15 Aug 2020
 * Description: various system tools for the bootloader
 *
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Provides Microprocessor Configuration settings.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */


#ifndef SYS_UTIL_H
#define SYS_UTIL_H

#include <stdbool.h>
#include <stdint.h>

#define   sys_get_sysclk()      (200000000)     /* SYSCLK frequency */
#define   sys_get_pbclk()       (100000000)     /* Generic (default) PBCLK1..6 = SYSCLK / 2 */

void      sys_init(void);

/* MCU soft reset */
void __attribute__((__noreturn__)) sys_reset(void);

/* global interrupt flag control */
void      sys_ie_disable(uint32_t *ieflag);
void      sys_ie_enable(uint32_t const ieflag);

/* timer */
uint32_t  sys_millisec_time(void);
bool      sys_timeout_ms(uint32_t const startTime, uint32_t const timeout);

/* address manipulation */
void     *sys_addr_to_phy(void const *addr);

#endif /* SYS_UTIL_H */
