/*
 * File  : main.c
 * Author: Cirilo Bernardo (cirilo.bernardo@gmail.com)
 * Date  : 14 Aug 2020
 * Description: bootloader main entry function
 *
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Provides Microprocessor Configuration settings.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#include <xc.h>
#include <stdbool.h>
#include <stdint.h>

#include "sys_util.h"
#include "uart.h"
#include "console.h"

static void (*chain) (void) = (void (*)(void))0xBD000000;   /* startup entry point for main */

/**
 * @Function main
 * @detail   Entry point for bootloader. Note: XC32 compilers don't allow void main(void)
 *           even though this function is never intended to return.
 */
int main(void)
{
    /* note: interrupts are not yet enabled at this point */

    /* PRECON - Set up prefetch */
    PRECONbits.PFMSECEN = 0;    /* Flash SEC Interrupt Enable (0 = disabled) */
    PRECONbits.PREFEN = 0b11;   /* Predictive Prefetch Enable (b11 = enabled for any address) */
    PRECONbits.PFMWS = 0b011;   /* PFM Wait States (3 cycles, Erratum 38 DS80000663L, Silicon Rev B2) */

    /* Enable KSEG0 cache */
    uint32_t cp0 = _mfc0(16, 0);
    cp0 &= ~0x07UL;
    cp0 |= 0b011UL; /* K0 = Cacheable, non-coherent, write-back, write allocate */
    _mtc0(16, 0, cp0);

    /* set the shadow register sets for lower interrupt latency */
    PRISSbits.PRI7SS = 7;
    PRISSbits.PRI6SS = 6;
    PRISSbits.PRI5SS = 5;
    PRISSbits.PRI4SS = 4;
    PRISSbits.PRI3SS = 3;
    PRISSbits.PRI2SS = 2;
    PRISSbits.PRI1SS = 1;

    /* select multi-vectored interrupts and enable the global interrupt flag */

    /* Set CP0.CAUSE.IV = 1 */
    unsigned int cause = _CP0_GET_CAUSE();
    cause |= 0x00800000;
    _CP0_SET_CAUSE(cause);
    _ehb();

    /* Select multi-vectored interrupts */
    INTCONbits.MVEC = 1;

    sys_ie_enable(1);
    sys_init();

    /* Send a message to the console */
    Console.Init();
    // Note: test that UART is working by sending "??\r\n"
    U2TXREG = '?';
    U2TXREG = '?';
    U2TXREG = '\r';
    U2TXREG = '\n';
    Console.Puts("** Bootloader initialized\n");

    /* Blinky 1 : flash LED for 0.2s every 2s */
    int32_t  phase = 0;
    uint32_t blinkTimer = sys_millisec_time();
    PORTHSET = (1 << 2);        /* green LED on */
    uint32_t chainTimer = 0;
    bool     nomain = false;    /* true if the main application was not detected */

    while (1)
    {
        console_task();
        Console.FlushRx();      /* dump received data */

        if ((false == nomain) && (0 == (PORTB & (1 << 12))))
        {
            if (0 == chainTimer)
            {
                chainTimer = sys_millisec_time();
            }
            else if (sys_timeout_ms(chainTimer, 500))
            {
                /* button pressed for 500ms; start the main app */
                uint32_t appTag = *((uint32_t *)0xBD000020);

                if (0x4E49414D == appTag)
                {
                    Console.Puts("** Chaining to main app\n");
                    Console.FlushTx();
                    Console.Uninit();
                    sys_ie_disable(0);
                    chain();                    /* jump to the startup routine for the main app */
                    __builtin_unreachable();    /* chain() cannot return since the stack is reset */
                }
            }
        }
        else
        {
            chainTimer = 0;
        }

        if (0 == phase)
        {
            if (sys_timeout_ms(blinkTimer, 200))
            {
                blinkTimer = sys_millisec_time();
                phase = 1;
                PORTHCLR = (1 << 2);    /* green LED off */
            }
        }
        else if (sys_timeout_ms(blinkTimer, 1800))
        {
            blinkTimer = sys_millisec_time();
            phase = 0;
            PORTHSET = (1 << 2);    /* green LED on */
        }
    }

    sys_reset();
    return 0;
}
