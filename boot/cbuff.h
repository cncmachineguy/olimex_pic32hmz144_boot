/*
 * File  : cbuff.h
 * Author: Cirilo Bernardo (cirilo.bernardo@gmail.com)
 * Date  : 15 Aug 2020
 * Description: circular buffers
 *
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Provides Microprocessor Configuration settings.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#ifndef CBUFF_H
#define CBUFF_H

#include <stdbool.h>
#include <stdint.h>

/* Available buffer sizes; note: size must be 2^n */
typedef enum
{
    CBUFF_SIZE_512 = 512,
} CBuffSize_e;

/* 512-byte buffer */
typedef struct
{
    uint32_t  head;
    uint32_t  tail;
    uint32_t  size;
    uint32_t  mask;
    uint8_t   data[512];
} CBuff_512;

/* Buffer manipulation functions */
typedef struct
{
    /**
     * @Function Init
     * @detail   Initializes a circular buffer.
     * @param    [IN] buffer is a pointer to an instance of a circular buffer.
     * @param    [IN] size is the size of the circular buffer.
     * @return   0 on success, else error code
     */
    int32_t  (*Init)(void *buffer, uint32_t size);

    /**
     * @Function Push
     * @detail   Stores a single byte in a buffer.
     * @param    [IN] buffer is a pointer to an instance of a circular buffer.
     * @param    [IN] token is the byte to store.
     * @return   0 on success, else error code
     */
    int32_t  (*Push)(void *buffer, uint8_t token);

    /**
     * @Function Pop
     * @detail   Retrieves a single byte from a buffer.
     * @param    [IN] buffer is a pointer to an instance of a circular buffer.
     * @return   0-255 on success, else error code
     */
    int32_t  (*Pop)(void *buffer);

    /**
     * @Function PushStr
     * @detail   Stores a string of bytes in a buffer.
     * @param    [IN] buffer is a pointer to an instance of a circular buffer.
     * @param    [IN] data_out is a pointer to the data to store.
     * @param    [IN] size is the number of bytes to store.
     * @return   the number of bytes stored, else error code. A return value less than
     *           size indicates a buffer overflow.
     */
    int32_t  (*PushStr)(void *buffer, void const *data_out, uint32_t const size);

    /**
     * @Function PopStr
     * @detail   Reads a string of bytes from a buffer.
     * @param    [IN]  buffer is a pointer to an instance of a circular buffer.
     * @param    [OUT] data_in is a pointer to receive the data.
     * @param    [IN]  size is the MAX number of bytes to read.
     * @return   on success the number of bytes read, else error code.
     */
    int32_t  (*PopStr)(void *buffer, void *data_in, uint32_t const size);

    /**
     * @Function GetSpace
     * @param    [IN]  buffer is a pointer to an instance of a circular buffer.
     * @return   number of free bytes in the buffer or else an error code.
     */
    int32_t  (*GetSpace)(void *buffer);

    /**
     * @Function GetByteCount
     * @param    [IN]  buffer is a pointer to an instance of a circular buffer.
     * @return   bytes stored in the buffer or else an error code.
     */
    int32_t  (*GetByteCount)(void *buffer);

    /**
     * @Function IsEmpty
     * @param    [IN]  buffer is a pointer to an instance of a circular buffer.
     * @return   true if the buffer is empty, not initialized, or invalid.
     */
    bool     (*IsEmpty)(void *buffer);
} const CBuff_t;

extern CBuff_t CBuff;

#endif /* CBUFF_H */
