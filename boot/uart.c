/*
 * File  : uart.c
 * Author: Cirilo Bernardo (cirilo.bernardo@gmail.com)
 * Date  : 15 Aug 2020
 * Description: UART driver
 *
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Provides Microprocessor Configuration settings.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#include <xc.h>
#include <sys/attribs.h>

#include "cbuff.h"
#include "cfg_interrupts.h"
#include "uart.h"
#include "sys_err.h"
#include "sys_util.h"

/******************************************************************************/
/*               PRIVATE VARIABLES                                            */
/******************************************************************************/
static CBuff_512     m_rxbuffer;
static CBuff_512     m_txbuffer;
static UartStatus_t  m_status;


/******************************************************************************/
/*               ISR FUNCTION DECLARATIONS                                */
/******************************************************************************/
void  __ISR(_UART2_RX_VECTOR, SRS_UART2_RX) UART2_RxHandler(void);


/******************************************************************************/
/*               PRIVATE FUNCTION DECLARATIONS                                */
/******************************************************************************/
static int32_t      uart2_init(uint32_t const bitrate);
static void         uart2_uninit(void);
static int32_t      uart2_send(void const *data, uint32_t const length);
static int32_t      uart2_receive(void *buffer, uint32_t const length);
static UartStatus_t uart2_status(void);
static void         uart2_flushtx(void);
static void         uart2_flushrx(void);
static bool         uart2_isactive(void);

/******************************************************************************/
/*               UART TASK AND PUBLIC API                                     */
/******************************************************************************/

void uart_task(void)
{
    if (0 == U2MODEbits.ON)
    {
        return; /* UART is not enabled */
    }

    int32_t txbytes = CBuff.GetByteCount(&m_txbuffer);

    while ((0 < txbytes) && (!U2STAbits.UTXBF))
    {
        int32_t token = CBuff.Pop(&m_txbuffer);

        if (0 <= token)
        {
            U2TXREG = (uint32_t)token;
        }
        else
        {
            /* inconsistent buffer: data may have been modified by an interrupt routine */
            break;
        }

        --txbytes;
    }

    if (0 != CBuff.GetSpace(&m_txbuffer))
    {
        m_status.tx_underrun = 1;
    }

    return;
}

UART_Driver UART2 =
{
    .Init = uart2_init,
    .Uninit = uart2_uninit,
    .Send = uart2_send,
    .Receive = uart2_receive,
    .Status = uart2_status,
    .FlushTx = uart2_flushtx,
    .FlushRx = uart2_flushrx,
    .IsActive = uart2_isactive,
};


/******************************************************************************/
/*               PRIVATE FUNCTION DEFINITIONS                                 */
/******************************************************************************/

static  int32_t  uart2_init(uint32_t const bitrate)
{
    U2MODE = 0;
    U2STA = 0;
    IEC4bits.U2RXIE = 0;
    int32_t result = 0;
    /* enable transmitter and receiver */
    U2STAbits.URXEN = 1;
    U2STAbits.UTXEN = 1;
    /* clear interrupt flag and set interrupt priorities */
    IFS4bits.U2RXIF = 0;
    IPC36bits.U2RXIP = IPL_UART2_RX;
    IPC36bits.U2RXIS = IPS_UART2_RX;
    /* set the bit rate */
    if (0 < bitrate)
    {
        uint32_t brg = ((sys_get_pbclk() + (8UL * bitrate)) / (16UL * bitrate)) - 1UL;

        if (65535 < brg)
        {
            result = E_PARAM;
        }
        else
        {
            U2BRG = brg;
        }
    }
    else
    {
        result = E_PARAM;
    }

    CBuff.Init(&m_rxbuffer, CBUFF_SIZE_512);
    CBuff.Init(&m_txbuffer, CBUFF_SIZE_512);

    if (0 == result)
    {
        /* enable the UART and its RX interrupt */
        U2MODEbits.ON = 1;
        IEC4bits.U2RXIE = 1;
    }

    return result;
} /* uart2_init */


static void  uart2_uninit(void)
{
    U2MODE = 0;
    U2STA = 0;
    IEC4bits.U2RXIE = 0;
    return;
} /* uart2_uninit */


static int32_t  uart2_send(void const *data, uint32_t const length)
{
    int32_t result = 0;

    if (0 != U2MODEbits.ON)
    {
        int32_t  txlen = CBuff.PushStr(&m_txbuffer, data, length);
        uint32_t txtimeout = sys_millisec_time();

        while ((0 <= txlen) && (txlen < (int32_t)length) && (!sys_timeout_ms(txtimeout, 500)))
        {
            while ((0 == CBuff.GetSpace(&m_txbuffer)) && (!sys_timeout_ms(txtimeout, 500)))
            {
                uart_task();
            }

            if (!sys_timeout_ms(txtimeout, 500))
            {
                uint8_t const *pdata = (uint8_t const *)data;
                int32_t tmp = CBuff.PushStr(&m_txbuffer, &pdata[txlen], length - (uint32_t)txlen);

                if (0 <= tmp)
                {
                    txlen += tmp;
                }
                else
                {
                    break;
                }
            }
        }

        result = txlen;

        if (0 == CBuff.GetSpace(&m_txbuffer))
        {
            m_status.tx_underrun = 0;
        }
    }
    else
    {
        result = E_UNAVAIL;
    }

    return result;
} /* uart2_send */


static int32_t  uart2_receive(void *buffer, uint32_t const length)
{
    int32_t result = 0;

    if (0 != U2MODEbits.ON)
    {
        if (m_status.rx_overflow)
        {
            /* report a failure so user can respond to the overflow */
            result = E_BUFFER;
        }

        result = CBuff.PopStr(&m_rxbuffer, buffer, length);
    }
    else
    {
        result = E_UNAVAIL;
    }

    return result;
} /* uart2_receive */


static  UartStatus_t uart2_status(void)
{
    if (0 == U2MODEbits.ON)
    {
        m_status.rx_overflow = 0;
        m_status.tx_underrun = 0;
    }

    return m_status;
} /* uart2_status */


static void uart2_flushtx(void)
{
    if (0 != U2MODEbits.ON)
    {
        uint32_t txtimer = sys_millisec_time();

        while ((!CBuff.IsEmpty(&m_txbuffer)) && (!sys_timeout_ms(txtimer, 5000)))
        {
            uart_task();
        }

        while ((!U2STAbits.TRMT) && (!sys_timeout_ms(txtimer, 5000)))
        {
            ;   /* wait for transmit shift register to complete its job */
        }
    }

    return;
} /* uart2_flushtx */

static void uart2_flushrx(void)
{
    if (0 != U2MODEbits.ON)
    {
        uint32_t ieflag = 0;
        sys_ie_disable(&ieflag);
        CBuff.Init(&m_rxbuffer, CBUFF_SIZE_512);
        m_status.rx_overflow = 0;

        /* resync if necessary */
        if (0 != U2STAbits.OERR)
        {
            IFS4bits.U2EIF = 0;
            U2STAbits.OERR = 0;
        }

        sys_ie_enable(ieflag);
    }

    return;
} /* uart2_flushrx */


static bool uart2_isactive(void)
{
    bool result = false;

    if ((0 != U2MODEbits.ON) && (0 != m_txbuffer.size) && (0 != m_rxbuffer.size))
    {
        result = true;
    }

    return result;
}

/******************************************************************************/
/*               UART RX INTERRUPT                                            */
/******************************************************************************/

/**
 * @Function UART2_RxHandler
 * @detail   Interrupt Service Routine for UART2.
 */
void  __ISR(_UART2_RX_VECTOR, SRS_UART2_RX) UART2_RxHandler(void)
{
    int32_t result = 0;

    while (0 != U2STAbits.URXDA)
    {
        uint8_t byte = (uint8_t)U2RXREG;

        if (0 == result)
        {
            result |= CBuff.Push(&m_rxbuffer, byte);
        }
    }

    if ((0 != result) && (0 < m_rxbuffer.size) && (0 == CBuff.GetSpace(&m_rxbuffer)))
    {
        m_status.rx_overflow  = 1;
    }

    IFS4bits.U2RXIF = 0;
    return;
}
