/*
 * File  : sys_err.h
 * Author: Cirilo Bernardo (cirilo.bernardo@gmail.com)
 * Date  : 15 Aug 2020
 * Description: error codes
 *
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Provides Microprocessor Configuration settings.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

typedef enum
{
    E_OK                        = 0L,           /* Success (no error) */
    E_FAIL                      = -1L,          /* General failure code */
    E_PARAM                     = -2L,          /* Invalid parameter */
    E_BUFFER                    = -3L,          /* buffer error due to being full or empty */
    E_UNAVAIL                   = -4L,          /* resource unavailable (not initialized) */
} SysError_e;
