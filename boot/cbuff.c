/*
 * File  : cbuff.c
 * Author: Cirilo Bernardo (cirilo.bernardo@gmail.com)
 * Date  : 15 Aug 2020
 * Description: circular buffers
 *
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Provides Microprocessor Configuration settings.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#include "cbuff.h"
#include "sys_err.h"


/******************************************************************************/
/*               PRIVATE TYPES                                                */
/******************************************************************************/

/* Buffer_t is generic and should not be instantiated */
typedef struct
{
    uint32_t  head;
    uint32_t  tail;
    uint32_t  size;
    uint32_t  mask;
    uint8_t   data[4];
} Buffer_t;


/******************************************************************************/
/*               PRIVATE VARIABLES                                            */
/******************************************************************************/


/******************************************************************************/
/*               PRIVATE FUNCTION DECLARATIONS                                */
/******************************************************************************/
static int32_t  cbuff_init(void *buffer, uint32_t size);
static int32_t  cbuff_push(void *buffer, uint8_t token);
static int32_t  cbuff_pop(void *buffer);
static int32_t  cbuff_pushstr(void *buffer, void const *data_out, uint32_t size);
static int32_t  cbuff_popstr(void *buffer, void *data_in, uint32_t size);
static int32_t  cbuff_getspace(void *buffer);
static int32_t  cbuff_getbytecount(void *buffer);
static bool     cbuff_isempty(void *buffer);

/******************************************************************************/
/*               PUBLIC API INSTANCE                                          */
/******************************************************************************/
CBuff_t CBuff =
{
    .Init = cbuff_init,
    .Push = cbuff_push,
    .Pop = cbuff_pop,
    .PushStr = cbuff_pushstr,
    .PopStr = cbuff_popstr,
    .GetSpace = cbuff_getspace,
    .GetByteCount = cbuff_getbytecount,
    .IsEmpty = cbuff_isempty,
};


/******************************************************************************/
/*               PRIVATE FUNCTION DEFINITIONS                                 */
/******************************************************************************/

static int32_t  cbuff_init(void *buffer, uint32_t size)
{
    int32_t result = 0;

    if ((0 == buffer) || (size != CBUFF_SIZE_512))
    {
        result = E_PARAM;
    }
    else
    {
        Buffer_t *pBuf = (Buffer_t *)buffer;
        uint32_t ieflag = __builtin_disable_interrupts();
        pBuf->head = 0;
        pBuf->tail = 0;
        pBuf->size = size;
        pBuf->mask = (size - 1UL);

        if (0 != ieflag)
        {
            __builtin_enable_interrupts();
        }
    }

    return result;
}

static int32_t  cbuff_push(void *buffer, uint8_t token)
{
    int32_t result = 0;

    if (0 == buffer)
    {
        /* not a valid buffer */
        result = E_PARAM;
    }
    else
    {
        Buffer_t *pBuf = (Buffer_t *)buffer;

        if (0 == pBuf->size)
        {
            /* buffer not initialized */
            result = E_PARAM;
        }
        else
        {
            if (((pBuf->head + 1UL) & pBuf->mask) != pBuf->tail)
            {
                uint8_t *data = (uint8_t *)pBuf->data;
                data[pBuf->head++] = token;
                pBuf->head &= pBuf->mask;
            }
            else
            {
                result = E_BUFFER;
            }
        }
    }

    return result;
}

static int32_t  cbuff_pop(void *buffer)
{
    int32_t result = 0;

    if (0 == buffer)
    {
        /* not a valid buffer */
        result = E_PARAM;
    }
    else
    {
        Buffer_t *pBuf = (Buffer_t *)buffer;

        if (0 == pBuf->size)
        {
            /* buffer not initialized */
            result = E_PARAM;
        }
        else
        {
            if (pBuf->head != pBuf->tail)
            {
                uint8_t *data = (uint8_t *)pBuf->data;
                result = data[pBuf->tail++];
                pBuf->tail &= pBuf->mask;
            }
            else
            {
                result = E_BUFFER;
            }
        }
    }

    return result;
}

static int32_t  cbuff_pushstr(void *buffer, void const *data_out, uint32_t const size)
{
    int32_t result = 0;

    if (0 == buffer)
    {
        /* not a valid buffer */
        result = E_PARAM;
    }
    else
    {
        Buffer_t *pBuf = (Buffer_t *)buffer;

        if (0 == pBuf->size)
        {
            /* buffer not initialized */
            result = E_PARAM;
        }
        else
        {
            /* maximum characters that can be pushed */
            uint32_t maxLen = (pBuf->size + pBuf->tail + ~(pBuf->head)) & pBuf->mask;

            if (maxLen > size)
            {
                maxLen = size;
            }

            /* copy as much as possible assuming that interrupts do not change the tail */
            uint32_t  head = pBuf->head;
            uint8_t  *dst = pBuf->data;
            uint8_t const *src = (uint8_t const *)data_out;

            while (0 < maxLen)
            {
                dst[head++] = *src++;
                head &= pBuf->mask;
                --maxLen;
                ++result;
            }

            pBuf->head = head;
        }
    }

    return result;
}

static int32_t  cbuff_popstr(void *buffer, void *data_in, uint32_t const size)
{
    int32_t result = 0;

    if (0 == buffer)
    {
        /* not a valid buffer */
        result = E_PARAM;
    }
    else
    {
        Buffer_t *pBuf = (Buffer_t *)buffer;

        if (0 == pBuf->size)
        {
            /* buffer not initialized */
            result = E_PARAM;
        }
        else
        {
            /* maximum characters that can be retrieved */
            uint32_t maxLen = (pBuf->head - pBuf->tail) & pBuf->mask;

            if (maxLen > size)
            {
                maxLen = size;
            }

            /* copy as much as possible assuming that interrupts do not change the head */
            uint32_t  tail = pBuf->tail;
            uint8_t  *src = pBuf->data;
            uint8_t  *dst = (uint8_t *)data_in;

            while (0 < maxLen)
            {
                *dst++ = src[tail++];
                tail &= pBuf->mask;
                --maxLen;
                ++result;
            }

            pBuf->tail = tail;
        }
    }

    return result;
}

static int32_t  cbuff_getspace(void *buffer)
{
    int32_t result = 0;

    if (0 == buffer)
    {
        /* not a valid buffer */
        result = E_PARAM;
    }
    else
    {
        Buffer_t *pBuf = (Buffer_t *)buffer;

        if (0 == pBuf->size)
        {
            /* buffer not initialized */
            result = E_PARAM;
        }
        else
        {
            result = (int32_t)((pBuf->size + pBuf->tail + ~(pBuf->head)) & pBuf->mask);
        }
    }

    return result;
}

static int32_t  cbuff_getbytecount(void *buffer)
{
    int32_t result = 0;

    if (0 == buffer)
    {
        /* not a valid buffer */
        result = E_PARAM;
    }
    else
    {
        Buffer_t *pBuf = (Buffer_t *)buffer;

        if (0 == pBuf->size)
        {
            /* buffer not initialized */
            result = E_PARAM;
        }
        else
        {
            result = (int32_t)((pBuf->head - pBuf->tail) & pBuf->mask);
        }
    }

    return result;
}

static bool cbuff_isempty(void *buffer)
{
    /* Note: if not initialized then cbuff is always empty and an attempt to push data will
       result in an error. */
    bool result = true;

    if (0 != buffer)
    {
        Buffer_t *pBuf = (Buffer_t *)buffer;

        if (pBuf->head != pBuf->tail)
        {
            result = false;
        }
    }

    return result;
}
