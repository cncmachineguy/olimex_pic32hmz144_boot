/*
 * File  : sys_util.h
 * Author: Cirilo Bernardo (cirilo.bernardo@gmail.com)
 * Date  : 15 Aug 2020
 *
 * Description: various system tools for the bootloader
 */


#ifndef SYS_UTIL_H
#define SYS_UTIL_H

#include <stdbool.h>
#include <stdint.h>

#define   sys_get_sysclk()      (200000000)     /* SYSCLK frequency */
#define   sys_get_pbclk()       (100000000)     /* Generic (default) PBCLK1..6 = SYSCLK / 2 */

void      sys_init(void);

/* timer */
uint32_t  sys_millisec_time(void);
bool      sys_timeout_ms(uint32_t const startTime, uint32_t const timeout);

/* address manipulation */
void     *sys_addr_to_phy(void const *addr);

#endif /* SYS_UTIL_H */
