/*
 * File  : main.c
 * Author: Cirilo Bernardo (cirilo.bernardo@gmail.com)
 * Date  : 14 Aug 2020
 *
 * Description: main application, main entry function
 */

#include <xc.h>

#include "sys_util.h"

extern unsigned int __builtin_enable_interrupts(void);
extern void _reset(void);

/**
 * @Function main
 * @detail   Entry point for main application. Note: XC32 compilers don't allow void main(void)
 *           even though this function is never intended to return.
 */
int main(void)
{
    /* note: interrupts are not yet enabled at this point */

    /* set the shadow register sets for lower interrupt latency */
    PRISSbits.PRI7SS = 7;
    PRISSbits.PRI6SS = 6;
    PRISSbits.PRI5SS = 5;
    PRISSbits.PRI4SS = 4;
    PRISSbits.PRI3SS = 3;
    PRISSbits.PRI2SS = 2;
    PRISSbits.PRI1SS = 1;

    /* select multi-vectored interrupts and enable the global interrupt flag */

    /* Set CP0.CAUSE.IV = 1 */
    unsigned int cause = _CP0_GET_CAUSE();
    cause |= 0x00800000;
    _CP0_SET_CAUSE(cause);
    _ehb();

    /* Select multi-vectored interrupts */
    INTCONbits.MVEC = 1;

    __builtin_enable_interrupts();
    sys_init();

    /* Blinky 2 : flash LED for 0.2s every 0.5s */
    int32_t  phase = 0;
    uint32_t blinkTimer = sys_millisec_time();
    PORTHSET = (1 << 2);    /* green LED on */
    
    while (1)
    {
        if (0 == phase)
        {
            if (sys_timeout_ms(blinkTimer, 200))
            {
                blinkTimer = sys_millisec_time();
                phase = 1;
                PORTHCLR = (1 << 2);    /* green LED off */
            }
        }
        else if (sys_timeout_ms(blinkTimer, 300))
        {
            blinkTimer = sys_millisec_time();
            phase = 0;
            PORTHSET = (1 << 2);    /* green LED on */
        }
    }

    _reset();
    __builtin_unreachable();    /* reset() does not return */
    return 0;
}
